---
title: "Guillaume Lodi"
date: 2018-10-13
publishdate: 2018-10-13
---

# Guillaume Lodi

## Developpeur

Peut-être es-tu un visiteur curieux, un recruteur ou encore un admirateur. Dans tous les cas, soit le bienvenue sur mon site.

Vous pourrez y trouver mon [blog](/blog), [mes experiences](/experiences), et [mes realisations](/realisation).

Bonne visite 👋
